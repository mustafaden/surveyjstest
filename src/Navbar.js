import React from 'react'
import { Link } from "react-router-dom";

function Navbar() {
    return (
        <>
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="/">
                            SurveyJS + ReactJS
                        </a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li>
                            <Link to="/test">Test</Link>
                        </li>
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/survey">Survey</Link>
                        </li>
                        <li>
                            <Link to="/creator">SurveyJS Creator</Link>
                        </li>
                        <li>
                            <Link to="/export">Export to PDF</Link>
                        </li>
                        <li>
                            <Link to="/analytics">Analytics</Link>
                        </li>
                        <li>
                            <Link to="/analyticstabulator">Results Table</Link>
                        </li>
                        <li>
                            <Link to="/analyticsdatatables">
                                Results Table (IE Support)
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar