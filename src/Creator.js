import React from "react";
import CreatorComponent from "./SurveyCreator";

export function CreatorPage() {
    return (
      <div>
        <h2>Survey Creator - create a new survey</h2>
        <CreatorComponent />
      </div>
    );
  }
  