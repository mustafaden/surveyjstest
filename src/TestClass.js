import React, { Component } from "react";
import * as Survey from "survey-core";
import * as SurveyReact from "survey-react-ui";
//Import localization
import "survey-core/survey.i18n.js";
//Import Survey styles
import "survey-core/defaultV2.css";

//import { json } from "./survey_json.js";

Survey.StylesManager.applyTheme("defaultV2");

let json = {
  pages: [
    {
      name: "customerContact", elements: [
        { type: "text", name: "name", title: "Please enter your name:" },
        { type: "text", name: "email", title: "Please enter your e-mail:" }
      ]
    }
  ]
};

export class SurveyClassComponent extends Component {
  constructor() {
    super();
    this.survey = new Survey.Model(json);
    this.survey.focusFirstQuestionAutomatic = true;
    this.survey.onValueChanged.add((sender, options) => {
      console.log("value changed!");
    });
    this.survey.onComplete.add((sender, options) => {
      console.log("Complete! Response:" + JSON.stringify(sender.data));
    });
  }
  render() {
    return (
      <div>
        <h3>This is Class based Survey</h3>
        <SurveyReact.Survey model={this.survey} />
      </div>
    )
  }
}
