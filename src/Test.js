import React from "react";
import { SurveyFuncComponent } from "./TestFunc";
import {SurveyClassComponent} from "./TestClass";

export function TestPage() {
    return (
    <div className="container">
    <h2>SurveyJS Library - two sample surveys below</h2>
    <SurveyClassComponent />
    <SurveyFuncComponent />
    </div>
    );
  }
  