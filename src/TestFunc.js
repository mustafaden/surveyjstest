import { useCallback } from "react";

import "survey-react/modern.min.css";
import "survey-react/survey.min.css";
import "survey-react/defaultV2.min.css";
// import 'survey-react/survey.min.css';
import { Survey, StylesManager, Model } from "survey-react";

StylesManager.applyTheme("modern");

const surveyJson = {
  elements: [
    {
      name: "FirstName",
      title: "Enter your first name:",
      type: "text"
    },
    {
      name: "LastName",
      title: "Enter your last name:",
      type: "text"
    }
  ]
};

export function SurveyFuncComponent() {
  const survey = new Model(surveyJson);
  survey.focusFirstQuestionAutomatic = true;
  const alertResults = useCallback((sender) => {
    const results = JSON.stringify(sender.data);
    alert(results);
  }, []);
  
  const valueChanger = () => {
    console.log("value changed!");
  }
  survey.onValueChanged.add(valueChanger);
  survey.onComplete.add(alertResults);

  return (
    <div>
      <h3>This is Function based Survey</h3>
      <Survey model={survey} />;
    </div>
  )
}

